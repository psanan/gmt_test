#!/usr/bin/env sh

module load gcc/6.3.0 openmpi/4.0.2 gmt libtiff
module load valgrind
