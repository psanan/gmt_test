#include <gmt.h>
#include <assert.h>

int main()
{
  void   *API;
  double wesn[6];
  double inc[2];
  struct GMT_GRID *grd;
  double *rdat;

  const char *fileout = "outputfile";
  const double xmin = 0.0;
  const double xmax = 1.234;
  const double ymin = -0.2;
  const double ymax = 1.2343;
  const int nx = 10;
  const int ny = 11;

  /* Initialize the GMT session */
  API = GMT_Create_Session ("writegrd", 2U, 0, NULL);

  /* Synthetic data */
  rdat = (double*) malloc(nx*ny*sizeof(double));
  for (size_t i=0; i< ((size_t) nx*ny); ++i) rdat[i] = i * 1.234;

  /* Create a grid object */
  wesn[GMT_XLO] = xmin;
  wesn[GMT_XHI] = xmax;
  wesn[GMT_YLO] = ymin;
  wesn[GMT_YHI] = ymax;
  wesn[GMT_ZLO] = -0.3;
  wesn[GMT_ZHI] = 0.7;
  inc[0] = (xmax-xmin)/((double)nx);
  inc[1] = (ymax-ymin)/((double)ny);

  grd = GMT_Create_Data(
      API,
      GMT_IS_GRID,            /* family */
      GMT_IS_SURFACE,         /* geometry */
      GMT_CONTAINER_AND_DATA, /* mode */
      NULL,                   /* par */
      wesn,
      inc,
      GMT_GRID_PIXEL_REG,     /* registration */
      0,                      /* pad */
      NULL                    /* data */
      );

  // Manually copy the date across..
  // https://docs.generic-mapping-tools.org/6.3/devdocs/api.html?highlight=gmt_create_data#manipulate-grids
  assert((unsigned int) nx == grd->header->n_columns);
  assert((unsigned int) ny == grd->header->n_rows);

  for (unsigned int row = 0; row < grd->header->n_rows; row++) {
    for (unsigned int col = 0; col < grd->header->n_columns; col++) {
      int node = GMT_Get_Index (API, grd->header, row, col);
      grd->data[node] = rdat[row * nx + col]; // Assumes an ordering that may be incorrect!
    }
  }

  free(rdat);


  /*  Write the file */
  GMT_Write_Data(
      API,
      GMT_IS_GRID,             /* family */
      GMT_IS_FILE,             /* method */
      GMT_IS_SURFACE,          /* geometry */
      GMT_CONTAINER_AND_DATA,  /* mode */
      wesn,
      fileout,
      grd
      );


  /* Destroy the GMT session */
  GMT_Destroy_Session(API);
}
