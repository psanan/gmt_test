runme : runme.c
	mpicc -g -Wall -Wextra -I$(GMT_ROOT)/include/gmt -L$(GMT_ROOT)/lib64 -lgmt -L$(LIBTIFF_ROOT)/lib -ltiff $< -o $@

clean: 
	rm -f runme *.o outputfile

.PHONY: clean all
